import pytest
from main import Html2MdBody, Md2GmiBody


@pytest.fixture
def default_md_body() -> Md2GmiBody:
    body = Md2GmiBody()
    body.markdown = "# here is the heading"
    return body


@pytest.fixture
def default_html_body() -> Html2MdBody:
    body = Html2MdBody()
    body.html = "<h1>Here is the heading</h1><p>here is the paragraph that follows it"
    return body
