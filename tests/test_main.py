import pytest
from main import html2md, md2gmi


@pytest.mark.asyncio
async def test_html2md(default_html_body):
    md = await html2md(default_html_body)
    assert md['result'] == "# Here is the heading\n\nhere is the paragraph that follows it\n\n"


@pytest.mark.asyncio
async def test_md2gmi(default_md_body):
    gmi = await md2gmi(default_md_body)
    assert gmi['result'] == "# here is the heading"
