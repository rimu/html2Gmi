from markdownify import markdownify
from md2gemini import md2gemini
from pathlib import Path
from fastapi import FastAPI, HTTPException
from typing import Union, Set
from pydantic import BaseModel


class Html2MdBody(BaseModel):
    html: Union[str, None] = ''
    strip: Set[str] = set()
    convert: Set[str] = set()
    autolinks: Union[bool, None] = True
    escape_underscores: Union[bool, None] = True
    escape_asterisks: Union[bool, None] = True
    default_title: Union[bool, None] = False
    heading_style: Union[str, None] = 'ATX'


class Md2GmiBody(BaseModel):
    markdown: Union[str, None] = ''
    code_tag: Union[str, None] = None
    img_tag: Union[str, None] = None
    indent: Union[str, None] = None
    ascii_table: Union[bool, None] = None
    frontmatter: Union[bool, None] = None
    jekyll: Union[bool, None] = None
    links: Union[str, None] = None
    plain: Union[bool, None] = None
    strip_html: Union[bool, None] = None
    base_url: Union[str, None] = None
    md_links: Union[bool, None] = None
    table_tag: Union[str, None] = None
    checklist: Union[bool, None] = None


class Html2Gmi(BaseModel):
    html: str
    html2md_options: Union[Html2MdBody, None] = Html2MdBody()
    md2gmi_options: Union[Md2GmiBody, None] = Md2GmiBody()


app = FastAPI()


@app.get('/')
def index():
    return {'result': 'Hello World'}


@app.post('/html2md/')
async def html2md(body: Html2MdBody):
    if body.html == '':
        raise HTTPException(status_code=400, detail='No HTML was provided')
    strip = list(body.strip) if len(body.strip) else None
    convert = list(body.convert) if len(body.convert) else None
    try:
        return {'result': markdownify(body.html, heading_style=body.heading_style, strip=strip, convert=convert,
                                      autolinks=body.autolinks, default_title=body.default_title,
                                      escape_underscores=body.escape_underscores, escape_asterisks=body.escape_asterisks)}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@app.post('/md2gmi/')
async def md2gmi(body: Md2GmiBody):
    if body.markdown == '':
        raise HTTPException(status_code=400, detail='No Markdown was provided')
    params = {'markdown': body.markdown}
    if body.code_tag is not None:
        params['code_tag'] = body.code_tag
    if body.img_tag is not None:
        params['img_tag'] = body.img_tag
    if body.indent is not None:
        params['indent'] = body.indent
    if body.ascii_table is not None:
        params['ascii_table'] = body.ascii_table
    if body.frontmatter is not None:
        params['frontmatter'] = body.frontmatter
    if body.jekyll is not None:
        params['jekyll'] = body.jekyll
    if body.links is not None:
        params['links'] = body.links
    if body.plain is not None:
        params['plain'] = body.plain
    if body.strip_html is not None:
        params['strip_html'] = body.strip_html
    if body.base_url is not None:
        params['base_url'] = body.base_url
    if body.md_links is not None:
        params['md_links'] = body.md_links
    if body.table_tag is not None:
        params['table_tag'] = body.table_tag
    if body.checklist is not None:
        params['checklist'] = body.checklist

    try:
        return {'result': md2gemini(**params)}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


@app.post('/html2gmi/')
async def html2gmi(body: Html2Gmi):

    # generate markdown from html
    body.html2md_options.html = body.html
    markdown = await html2md(body.html2md_options)

    # turn markdown into gemtext. NB the "paragraph" link mode is probably best to use https://mastodon.nzoss.nz/web/@rysiek@mastodon.technology/108727228200756181
    body.md2gmi_options.markdown = markdown['result']
    gmi = await md2gmi(body.md2gmi_options)

    return {'result': gmi['result']}

if __name__ == '__main__':
    sample_html = Path('sample.html').read_text()
    md = markdownify(sample_html, heading_style='ATX')
    print(md2gemini(md, links='copy'))
